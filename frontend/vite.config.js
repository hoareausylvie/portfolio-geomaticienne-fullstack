import { fileURLToPath, URL } from 'node:url'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import VueDevTools from 'vite-plugin-vue-devtools'

import Dotenv from 'dotenv-webpack'
import purgecss from '@fullhuman/postcss-purgecss'

// import process from 'process';
// Variable globale process.env

// process.define('process.env', JSON.stringify({
//   NODE_ENV: 'production',
//   VUE_APP_API_URL: process.env.VUE_APP_API_URL
// }));

// eslint-disable-next-line no-undef
// globalThis.process = {
//   env: {
//     NODE_ENV: 'production',
//     VUE_APP_API_URL: process.env.VUE_APP_API_URL || 'http://localhost:3000/api'
//   }
// }

const production = process.env.NODE_ENV === 'production';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VueDevTools(),
  ],
  base: process.env.BASE_URL,
  build: {
    outDir: 'dist', // Dossier de sortie pour les fichiers
    assetsDir: 'assets', // Répertoire des assets à la racine
    rollupOptions: {
      input: {
        main: fileURLToPath(new URL('index.html', import.meta.url))
      }
    },
    sourcemap: false, // Désactive les sourcemaps
    minify: 'terser', // Utilise Terser pour minifier
    terserOptions: {
      compress: {
        drop_console: true, // Supprime les consoles logs
        drop_debugger: true // Supprime les débogueurs
      }
    }
  },
  cssCodeSplit: false,
  cssTarget: 'chrome80',
  postcss: './postcss.config.js',
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  define: {
    'process.env': {
      VUE_APP_API_URL: process.env.VUE_APP_API_URL || 'http://localhost:3000/api'
    }
  },
  transpileDependencies: true,
  publicDir: 'public', // Dossier public pour les ressources statiques
  // publicPath: publicPath(), // Chemin public en fonction de l'URL de Gitlab Pages
  configureWebpack: {
    plugins: [
      new Dotenv()
    ]
  }
})
