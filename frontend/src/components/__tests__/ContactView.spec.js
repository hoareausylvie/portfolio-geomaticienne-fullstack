import { describe, it, expect, vi } from 'vitest';
import { mount } from '@vue/test-utils';
import ContactView from '@/views/ContactView.vue';

describe('ContactView', () => {
    it('should submit the form and display success message', async() => {
        const wrapper = mount(ContactView);

        await wrapper.find('input[name="name"]').setValue('John');
        await wrapper.find('input[name="surname"]').setValue('Doe');
        await wrapper.find('input[name="email"]').setValue('john.doe@example.com');
        await wrapper.find('textarea[name="message"]').setValue('Hello, this is a test message.');

        // Simuler la réponse du serveur
        window.fetch = vi.fn(() => 
            Promise.resolve({
                ok: true,
                json: () => Promise.resolve({ message: 'Success' })
            })
        );

        // Soumettre le formulaire
        await wrapper.find('button[type="submit"]').trigger('click');

        // Vérifier le texte de l'élément de message de succès
        const successMessage = wrapper.find('p.success-message');
        expect(successMessage.text()).toBe('Votre message a été envoyé avec succès.');
    })
})