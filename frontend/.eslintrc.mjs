/* eslint-env node */
import eslintPatch from '@rushstack/eslint-patch/modern-module-resolution';

eslintPatch();

export default {
  root: true,
  'extends': [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    '@vue/eslint-config-prettier/skip-formatting'
  ],
  overrides: [
    {
      files: [
        'e2e/**/*.{test,spec}.{js,ts,jsx,tsx}'
      ],
      'extends': [
        'plugin:playwright/recommended'
      ]
    }
  ],
  parserOptions: {
    ecmaVersion: 'latest'
  }, 
  env: {
    node: true, // Pour utiliser Node.js
  },
  globals: {
    process: 'readonly' // 'process' est une variable globale en lecture seule
  }
}
