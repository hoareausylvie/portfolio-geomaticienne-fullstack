// server/config.js
import dotenv from 'dotenv';
import path from 'path';
import process from 'process';

// Détermine le fichier .nev à utiliser en fonction de NODE_ENV
const envFile = process.env.NODE_ENV === 'production' ? '.env.production' : '.env';
dotenv.config({ path: path.resolve(process.cwd(), envFile) });

const config = {
  hostname: process.env.HOST,
  port: process.env.PORT,
};

export default config;