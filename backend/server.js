import express from 'express';
import https from 'https';
import fs from 'fs';
import { app } from './src/index.js';
import dotenv from 'dotenv';
import process from 'process';

// Charger les variables d'environnement dans le fichier .env
dotenv.config();

// Charger les certificats SSL
const options = {
    key: fs.readFileSync(process.env.SSL_KEY_PATH),
    cert: fs.readFileSync(process.env.SSL_CERT_PATH)
}

// Utiliser les fichiers public
app.use(express.static('public'));

app.get('/api', (req, res) => {
    res.send('Hello from the backend !');
});

// Port découte du serveur pour la production
const PORT_PROD = process.env.PORT_PROD || 443;

// Pour démarrer le serveur HTTPS en production
https.createServer(options, app).listen(PORT_PROD, () => {
    console.log(`Server HTTPS démarré sur le port : ${PORT_PROD}`);
});
