// tests/protectedRoutes.test.js
import { expect } from "chai";
import sinon from "sinon";
import request from "supertest";
import { app } from "../src/index.js";
import jwt from "jsonwebtoken";
import User from "../src/models/User.js";
import authMiddleware from "../src/middlewares/authMiddleware.js";
import { getAllMessages, deleteMessage } from "../src/controllers/messageController.js";

describe("Protected Routes", () => {
   let sandbox;

  beforeEach(() => {
    // Créer la sandbox
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    // Réinitialiser les mocks avant chaque test
    sandbox.restore();
  });

  it("should return 200 and all messages when authenticated", async () => {
    const userId = "valid_user_id";
    const username = "valid_username";
    const accessToken = jwt.sign({ userId }, process.env.ACCESS_TOKEN_SECRET);
    
    // Stub the authMiddleware to bypass authentication
    sandbox.stub(User, 'findById').resolves(new User({ _id: userId, username }));

    const res = await request(app)
      .get("/protected/messages")
      .set('Cookie', `token=${accessToken}`);

    expect(res.status).to.equal(200);
    expect(res.body).to.be.an('array');
  });



  // it("should return 401 when authenticated for /messages route", async () => {
  //   authMiddleware.mockImplementation((req, res) =>
  //     res.status(401).json({ message: "Unauthorized" })
  //   );

  //   const res = await request(app).get("/protected/messages");

  //   expect(authMiddleware).to.have.been.called;
  //   expect(getAllMessages).not.to.have.been.called;
  //   expect(res.status).to.equal(200);
  //   expect(res.body).to.deep.equal({ message: "Unauthorized" });
  // });

  // it("should return 200 and delete the message when authenticated", async () => {
  //   authMiddleware.mockImplementation((req, res, next) => next());
  //   deleteMessage.mockImplementation((req, res) =>
  //     res.status(200).json({ messageId: 1 })
  //   );

  //   const res = await request(app).delete("/protected/1");

  //   expect(authMiddleware).to.have.been.called;
  //   expect(deleteMessage).not.to.have.been.called;
  //   expect(res.status).to.equal(200);
  //   expect(res.body).to.deep.equal({ messageId: 1 });
  // });

  // it("should return 401 when not authenticated for delete route", async () => {
  //   authMiddleware.mockImplementation((req, res) =>
  //     res.status(401).json({ message: "Unauthorized" })
  //   );

  //   const res = await request(app).delete("/protected/1");

  //   expect(authMiddleware).to.have.been.called;
  //   expect(deleteMessage).not.to.have.been.called;
  //   expect(res.status).to.equal(401);
  //   expect(res.body).to.deep.equal({ messages: "Unauthorized" });
  // });

  // it("should clear cookie and redirect to home on logout", async () => {
  //   authMiddleware.mockImplementation((req, res) => {
  //       // Supprime le cookie de jeton d'authentification
  //       res.clearCookie("token");
  //       // Redirige vers la page d'accueil
  //       res.redirect("/");
  //   });
    
  //   const res = await request(app).delete("/protected/logout");

  //   expect(authMiddleware).to.have.been.called;
  //   expect(res.headers["set-cookie"][0]).to.match("/token=;/");
  //   expect(res.headers.location).to.equal("/");
  //   expect(res.status).to.equal(302);
  // });
});

