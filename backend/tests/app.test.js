// tests/app.test.js
import request from 'supertest';
import { expect } from 'chai';
import { app } from '../src/index.js';

describe('App Integration Tests', () => {
    describe('GET /', () => {
        it('should render the index page', async() => {
            const res = await request(app).get('/');
            expect(res.statusCode).to.equal(200);
            expect(res.text).to.contain('<title>Accueil</title>');
        });
    
        it('should return 404 for unknown routes', async() => {
            const res = await request(app).get('/unknown-route');
            expect(res.statusCode).to.equal(404);
            expect(res.text).to.contain('404');
        });
    
    });
});
