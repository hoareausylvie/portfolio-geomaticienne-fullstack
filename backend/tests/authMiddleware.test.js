// tests/authMiddleware.test.js
import * as chai from "chai";
import chaiHttp from "chai-http";
import sinon from "sinon";
// import sinonChai from "sinon-chai";
import jwt from "jsonwebtoken";
import express from "express";
import supertest from "supertest";
import authMiddleware from "../src/middlewares/authMiddleware.js";
import User from "../src/models/User.js";
import dotenv from "dotenv";

dotenv.config(); // Charge les variables d'environnement depuis le fichier .env

chai.use(chaiHttp);
// chai.use(sinonChai);

const { expect } = chai;

// Mock des méthode de User
const userFindOneStub = sinon.stub(User, "findOne");

// Fonction pour générer les tokens
const generateToken = (userId) =>
  jwt.sign({ userId }, process.env.ACCESS_TOKEN_SECRET);

console.log("Début du test authMiddleware");

describe("authMiddleware", () => {
  let app;

  beforeEach(() => {
    app = express();

    app.use((req, res, next) => {
      req.cookies = {}; // Pour simuler les cookies
      next();
    });
    app.use(authMiddleware);
    app.get("/protectedRoute", (req, res) =>
      res.status(200).json({ message: "Success", user: req.user })
    );
  });

  // Restaure sinon après chaque test
  afterEach(() => {
    sinon.reset();
  });

  // Test 1 - Pas de token
  it("should return a 401 status if no token is provided", (done) => {
    supertest(app)
      .get("/protectedRoute")
      .end((err, res) => {
        expect(res).to.have.status(401);
        done();
      });
  });

  // Test 2 - Utilisateur non trouvé
  it("should return 401 if the user is not found", async () => {
    const req = {
      headers: {
        authorization: "Bearer validToken",
      },
    };

    const res = await supertest(app).get("/protectedRoute").set(req.headers);
    expect(res).to.have.status(401);
    expect(res.text).to.equal("Unauthorized ! User not found.");

  });

  // Test 3 - Utilisateur et token valides
  it("should return 200 and user if token is valid", async () => {
    const token = "Bearer validToken";
    const res = await request(app)
      .get("/protectedRoute")
      .set("Authorization", token);
    expect(res).to.have.status(200);
    eexpect(res.text).to.equal( "Unauthorized ! Token expired.");
  });

  // Test 4 - Token expiré

  it("should return 401 if token is expired", async () => {
    const expiredToken = "Bearer expiredToken";
    const res = await supertest(app)
      .get("/protectedRoute")
      .set("Authorization", expiredToken);
    expect(res).to.have.status(401);
    expect(res.text).to.equal("Unauthorized ! Token expired.");
  });

  // Test 5 - Token invalide
  it("should return 403 if token is invalid", async () => {
    const invalidToken = "invalidToken";

    const res = await supertest(app)
      .get("/protectedRoute")
      .set("Aithorization", invalidToken);
    expect(res).to.have.status(403);
    expect(res.text).to.equal("Forbidden ! Invalid token.");
  });

// Test 6 - Erreur interne

it("should return 500 for other internal server error", (done) => {
  userFindOneStub.rejects(new Error("Some database error"));

  const token = generateToken("errorUserId");

  supertest(app)
    .get("/protectedRoute")
    .set("Authorization", `Bearer ${token}`)
    .expect(500)
    .end((err, res) => {
      expect(res).to.have.status(500);
      done();
    });
  });
});
