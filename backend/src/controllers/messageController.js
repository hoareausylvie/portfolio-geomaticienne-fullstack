// controllers/messageController.js
import { fileURLToPath } from 'url';
import { dirname } from 'path'; // Pour gérer les chemins de fichiers
import Message from '../models/Message.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Pour lire tous les messages reçus
export const getAllMessages = async(req, res, next) => {
    try {
        // Récupérer le nom de l'utilisateur connecté
        const username = req.user?.username;

        const messages = await Message.find({});
        // Vérifier si le middleware d'authentification a déjà renvoyé une réponse
        if (res.headersSent) {
            return next();
        }
        // Rend la page HTML avec la liste des messages
        res.render('messages', { messages, username });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

// Pour ajouter un nouveau message depuis le frontend
// Il n'est pas nécessaire d'être authentifié pour afficher un message depuis le frontend
export const createMessage = async(req, res) => {
    try {
        const { name, surname, email, message } = req.body; // Les données du formulaire sont dans req.body

        // Créer une nouvelle instance du modèle Message
        const newMessage = new Message({
            name,
            surname,
            email,
            message,
        });

        // Sauvegarder le message dans la base de données et l'afficher dans le backend
        const savedMessage = await newMessage.save();
        // console.log('Données du formulaire sauvegardée : ', savedMessage);
        res.status(201).json({ message: 'Données du formulaire stockées avec succès !', savedMessage });
    } catch (error) {
        console.error('Une erreur est survenue lors de l\'enregistrement des données :', error);
        res.status(500).json({error: 'Une erreur est survenue lors de l\'enregistrement des données'});
    }
};

// Pour effacer un message depuis le backend
 // Il faut être authentifier pour effacer un message
export const deleteMessage = async(req, res) => {
    try {
        // Supprime le message de la base de données en fonction de  l'identifiant du message
        const messageId = req.params.id;
        const message = await Message.findByIdAndDelete(messageId);

        if (!message) {
            return res.status(404).json({ message: 'Message not found' });
        }

        res.status(200).json({ success: true, message: 'Le message a bien été supprimé.' });
    } catch (error) {
        res.status(500).json({ success: false, message: "Une erreur est survenue lors de la suppression du message.", error});
    }
};
