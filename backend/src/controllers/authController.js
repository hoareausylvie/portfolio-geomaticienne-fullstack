// controllers/authController.js
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import bcrypt from 'bcrypt';
import User from '../models/User.js';
import process from 'process';

dotenv.config();

// console.log('ACCESS_TOKEN_SECRET', process.env.ACCESS_TOKEN_SECRET);

// Contrôleur pour la gestion de l'authentification
const authController = {
    // Pour authentifier un utilisateur
    login: async (req, res) => {
        try {
            // Récupérer les identifiants de l'utilisateur
            const { username, password } = req.body;

            // console.log('Username:', username);
            // console.log('Password:', password);

            // Vérifier que les identifiants sont fournis
            if (!username || !password) {
                return res.status(400).json({ message: 'Identifiants manquants '});
            }

            // Récupérer l'utilisateur à partir de la base de données
            const user = await User.findOne({ username });

            // console.log('User:', user);

            // Vérifier que l'utilisateur existe
            if (!user) {
                return res.status(401).json({ message: 'Utilisateur inconnu' });
            }

            // Vérifier que le mot de passe est valide
            const isPasswordValid = await bcrypt.compare(password, user.password);

            // console.log('isPasswordValid:', isPasswordValid);

            if (!isPasswordValid) {
                return res.status(401).json({ message: 'Mot de passe incorrect' });
            }

            // Générer un jeton d'accès
            const accessToken = jwt.sign({ userId: user._id}, process.env.ACCESS_TOKEN_SECRET, {
                expiresIn: '2h',
            });

            // console.log('AccessToken:', accessToken);

            // Enregistre le token dans la base de données pour cet utilisateur
            user.tokens = user.tokens.concat({ token: accessToken });
            await user.save();

            // Stocker le jeton d'accès dans un cookie HTTP-only pour une sécurité accrue
            res.cookie('token', accessToken, { httpOnly: true, secure: false }); // Mettre true ene production

            // Rediriger l'utilisateur vers la page des messages
            res.redirect('/protected/messages');

            // console.log('Response:', res.json);
        } catch (error) {
            res.status(500).json({ message: error.message });
            // Rediriger l'utilisateur vers la page d'accueil pour une authentification
            res.redirect('/');
        }
    },
    // Pour inscrire un nouvel utilisateur
    signup: async (req, res) => {
        try {
            // Récupérer les informations de l'utilisateur
            const { username, password } = req.body;

            // Vérifier que les informations sont fournies
            if (!username || !password) {
                return res.status(400).json({ message: 'Informations manquantes' });
            }

            // Vérifier si l'utilisateur existe déjà
            const existingUser = await User.findOne({ username });
            if (existingUser) {
                return res.status(400).json({ message: 'Nom d\'utilisateur déjà pris' });
            }

            // Hacher le mot de passe
            const hashedPassword = await bcrypt.hash(password, 10);

            // Créer un nouvel utilisateur
            const newUser = new User({ username, password: hashedPassword });
            await newUser.save();

            // Renvoyer une réponse de succés
            res.status(201).json({ message: 'Inscription réussie'});
        } catch (error) {
            console.error('Erreur during signup:', error);
            res.satus(500).json({ message: error.message });
        }
    }
}

export default authController;
