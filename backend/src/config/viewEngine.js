// src/config/viewEngine.js

// Configuration du moteur de modèle EJS
const setViewEngine = (app, path, __dirname) => {
    app.set('view engine', 'ejs');
    app.set('views', path.join(__dirname, 'views')); // Défini le répertoire des vues
}

export default setViewEngine;
