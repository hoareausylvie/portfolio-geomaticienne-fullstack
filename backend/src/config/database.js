// config/database.js
import mongoose from 'mongoose';
import dotenv from 'dotenv';
// import path from 'path';
import process from 'process';

// Charger le fichier .env en fonction de NODE_ENV
// const envFile = process.env.NODE_ENV === 'development';
// dotenv.config({ path: path.resolve(process.cwd(), envFile) });

dotenv.config();

// Connexion à la base de données MongoDB
const connectDB = async () => {
    try {
        await mongoose.connect(process.env.BACKEND_MONGO_DB_URL, {
            // useNewUrlParser: true,
            // useUnifiedTopology: true,
            connectTimeoutMS: 30000, // 30s
        });
        console.log('MongoDB connected successfully');
    } catch (error) {
        console.error('MongoDB connected failed', error);
        process.exit(1);
    }
}

export default connectDB;
