// src/index.js
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import dotenv from 'dotenv';
import { fileURLToPath } from 'url';
import path, { dirname } from 'path'; // Pour gérer les chemins de fichiers
import cookieParser from 'cookie-parser';
import methodOverride from 'method-override';
import process from 'process';

import connectDB from './config/database.js';
import setViewEngine from './config/viewEngine.js';
import checkFirstVisit from './middlewares/checkFirstVisit.js';
import publicRoutes from './routes/publicRoutes.js';
import protectedRoutes from './routes/protectedRoutes.js';

// Initialisation
dotenv.config(); // Charge les variables d'environnement depuis le fichier .env
const app = express();

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Middlewares

// Middleware pour parser les données du formulaire
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

// Middleware pour activer CORS
app.use(cors());

// Middleware qui gère les cookies 
app.use(cookieParser());

// Middleware d'authentification
// app.use(authMiddleware);

// Middleware pour vérifier que c'est la première visite de l'utilisateur
app.use(checkFirstVisit);
app.use(methodOverride('_method'));

// Connexion à la base de données MongoDB
connectDB();

// Serveur les fichiers statiques à partir du répertoire de build
app.use('/assets', express.static(path.join(__dirname, 'assets')));

// Configuration du moteur de modèle EJS
setViewEngine(app, path, __dirname);

// Utilisation des routes
// Routes publiques
app.use('/public', publicRoutes);

// Routes protégées
app.use('/protected', protectedRoutes);

// Route pour la page d'accueil
app.get('/', async (req, res) => {
    try {
        // Rend la page HTML de la page d'accueil à partir du fichier index.ejs
        res.render('index');
    } catch (error) {
        res.status(500).json({ message: 'Une erreur est survenue lors du rendu de la page d\'accueil.', error });
    }
});

// Route pour la page de résultats
// app.get('/results', (req, res) => {
//     var results = getResults();
//     res.render('results', { results: results});
// });

// Gérer les erreurs 404
app.use((req, res, next) => {
    res.status(404);
    res.render('404');
    next();
})

// Port d'écoute du serveur
const PORT_DEV = process.env.PORT_DEV || 3000;
// Pour démarrer le serveur
app.listen(PORT_DEV, () => {
    console.log(`Serveur démarré sur le port ${PORT_DEV} à l'adresse http://localhost:${PORT_DEV}`);
});

export { app };