// routes/publicRoutes.js
import express from 'express';
import { createMessage } from '../controllers/messageController.js';
import authController from '../controllers/authController.js';

const router = express.Router();

// Routes accessibles sans authentification

// Autres routes publiques
router.get('/login', (req, res) => {
    res.render('login');
});

// Route POST pour la connexion
router.post('/login', authController.login);

router.get('/signup', (req, res) => {
    res.render('signup');
});

// Route POST pour l'inscriptionn
router.post('/signup', authController.signup);

// Route pour ajouter un nouveau message depuis le frontend
router.post('/submit', createMessage);

export default router;