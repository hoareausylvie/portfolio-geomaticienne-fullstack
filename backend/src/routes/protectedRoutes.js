// routes/protectedRoutes.js
import express from 'express';
import authMiddleware from '../middlewares/authMiddleware.js';
import { getAllMessages, deleteMessage } from '../controllers/messageController.js';

const router = express.Router();

// Routes protégées (nécessite une authentification)

// Route pour récupérer tous les messages depuis la base de données
router.get('/messages', authMiddleware, getAllMessages);
// router.get('/messages', getAllMessages);

// Route pour supprimer un message par son identifiant
router.delete('/:id', authMiddleware, deleteMessage);

// Route pour se déconnecter
router.get('/logout', authMiddleware, (req, res) => {
    // Supprime le cookie de jeton d'authentification
    res.clearCookie('token');
    // Redirige vers la page d'accueil
    res.redirect('/');
});

export default router;