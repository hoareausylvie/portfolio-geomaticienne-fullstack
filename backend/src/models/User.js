import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    tokens: [
        {
            token: { type: String, required: true }
        }
    ]
});

// Hachage du mot de passe avant de l'enregistrer dans la base de données
// userSchema.pre('save', async function(next) {
//     if (this.isModified('password')) {
//         this.password = await bcrypt.hash(this.password, 10);
//     }
//     next();
// })

const User = mongoose.model('User', userSchema);

export default User;