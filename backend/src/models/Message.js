import mongoose from "mongoose";
import validator from "validator";

// Schéma de la collection des messages
const messageSchema = new mongoose.Schema({
    name: { type: String, required: true },
    surname: { type: String, required: true },
    email: { 
        type: String, 
        required: true,
        validate: {
            validator: function(v) {
                return validator.isEmail(v);
            },
            message: props => `${props.value} n'est pas un email valide !`
        }
    },
    message: { type: String, required: true }
}, { timestamps: true });

// Modèle de la collection des messages
const Message = mongoose.model('Message', messageSchema);

export default Message;