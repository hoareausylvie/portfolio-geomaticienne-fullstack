// authMiddleware.js
import dotenv from "dotenv";
import jwt from "jsonwebtoken";
import User from "../models/User.js";
import process from 'process';

dotenv.config();

console.log("Appel de la fonction authMiddleware");

// Middleware pour l'authentification
const authMiddleware = async (req, res, next) => {
  try {
    // Récupérer le token depuis les headers, le cookie ou le corps de la requête
    // console.log("req.headers", req.headers);

    const authHeader = req.headers.authorization;
    let token;

    if (authHeader && authHeader.startsWith('Bearer ')) {
      token = authHeader.split(' ')[1];
    } else if (req.cookies && req.cookies.token) {
      token = req.cookies.token;
    }
   
    // console.log("token", token);

    if (!token) {
      return res
        .status(401)
        .json({ message: "Unauthorized ! No token provided." });
    }

    // Vérifier que le jeton d'accès est validé et le décoder
    const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    // console.log("DecodedToken:", decoded);

    // Chercher l'utilisateur correspondant dans la base de données
    const user = await User.findOne({
      _id: decoded.userId,
      "tokens.token": token,
    });
    // console.log("User:", user);

    if (!user) {
      return res
        .status(401)
        .json({ message: "Unauthorized ! User not found." });
    }

    // Vérifiez si le token fourni correspond à des tokens enregistrés pour cet utilisateur
    // const isValidToken = user.tokens.some(tok => tok.token === token);
    // if (!isValidToken) {
    //   return res
    //     .status(401)
    //     .json({ message: "Unauthorized ! Invalid token." });
    // }

    // Ajouter l'utilisateur à la requête pour l'utiliser dans les prochains middlewares
    req.user = user;

    // Passer au middleware suivant
    next();
  } catch (error) {
    console.error("Error during authentication:", error);

    // Gestion des erreurs JWT spécifiques
    if (error.name === "TokenExpiredError") {
      return res.status(401).json({ message: "Unauthorized ! Token expired." });
    }
  
    if (error.name === "JsonWebTokenError") {
      return res.status(403).json({ message: "Forbidden ! Invalid Token." });
    }
  
    // Erreur interne du serveur pour les autres cas
    return res.status(500).json({ message: "Internal Server Error." });
  }

};

export default authMiddleware;
