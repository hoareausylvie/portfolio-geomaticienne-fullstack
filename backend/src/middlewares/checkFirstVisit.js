// checkFirstVisit
// Middleware pour vérifier si c'est la première visite de l'utilisateur
const checkFirstVisit = (req, res, next) => {
    if (!req.cookies.hasVisitedBefore) {
        res.cookie('hasVisitedBefore', 'true');
        res.locals.isFirstVisit = true;
    } else {
        res.locals.isFirstVisit = false;
    }
    next();
}

export default checkFirstVisit;